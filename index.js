/* Remove empty p tags only from the end of html. Html 
could be any generic html. What would be your approach to it?
 Hint: This is related to dom manipulation. U can use RunJs for completing this task.
Example HTML */

var HTMLParser = require('node-html-parser');

let  htmlContent =`<body>
<div>
<p style="text-align:right">Gilmore &amp; Bell,
P.C.</p>
<p style="text-align:right">07/29/2019</p>
<p></p>
<p style="text-align:center">(Published in <b><i>The
Derby Informer </i></b> on August 21, 2019)</p>
<p></p>
<p style="text-align:center"><b><span>RESOLUTION NO. 13-2019</span></b></p>
<p></p>
<p><b><span>A
RESOLUTION DETERMINING THE ADVISABILITY OF THE MAKING OF CERTAIN INTERNAL
IMPROVEMENTS IN </span>THE CITY OF DERBY, KANSAS <span>; MAKING CERTAIN FINDINGS WITH RESPECT THERETO; AND AUTHORIZING AND
PROVIDING FOR THE MAKING OF THE IMPROVEMENTS IN ACCORDANCE WITH SUCH FINDINGS (WATER
LINE IMPROVEMENTS-PHASE 2  COURTYARDS AT THE OAKS 2 <span>ND </span> ADDITION).</span></b></p>
<div>
<p><span></span></p>
</div>
<p></p>
<p><b><span>WHEREAS </span></b>,
a petition (the Petition) was filed with the City Clerk of the City of Derby,
Kansas (the City) proposing certain internal improvements; and said Petition
sets forth:  (a) the general nature of the proposed improvements; (b) the
estimated or probable cost of the proposed improvements; (c) the extent of the
proposed improvement district to be assessed for the cost of the proposed
improvements; (d) the proposed method of assessment; (e) the proposed
apportionment of the cost between the improvement district and the City-at-large;
and (f) a request that such improvements be made without notice and hearing as
required by K.S.A. 12-6a01 <i><span>et seq. </span></i>;
and</p>
<p></p>
<p><b><span>WHEREAS </span></b>,
the governing body of the City hereby finds and determines that said Petition
was signed by the owners of record of more than one-half of the area liable for
assessment for the proposed improvements, and is therefore sufficient in
accordance with the provisions of K.S.A. 12-6a01 <i><span>et seq. </span></i> (the Act).</p>
<p></p>
<p><b><span>THEREFORE,
BE IT RESOLVED BY THE GOVERNING BODY OF THE CITY OF DERBY, KANSAS:</span></b></p>
<p></p>
<p><b><span>Section 1 </span></b>.  <b><span>Findings of Advisability </span></b>.  The
governing body hereby finds and determines that:</p>
<p></p>
<p>(a)  It is advisable to make the following
improvements (the Improvements):</p>
<p></p>
<p><span>Construction
of water lines (Phase 2) to serve the area described as the Improvement
District, all </span>in accordance with City of Derby Standards and plans and
specifications prepared or approved by the City Engineer <span>.</span></p>
<p></p>
<p>(b)  The estimated or probable cost of the
proposed Improvements is:  Thirty-Nine Thousand Dollars ($39,000.00), said
estimated amount to be increased at the pro rata rate of 1 percent per
month from and after the date of adoption of this Resolution.</p>
<p></p>
<p>(c)  The extent of the improvement
district (the Improvement District) to be assessed for the cost of the
Improvements is:</p>
<p></p>
<p><a>Lots 1 through 12,
inclusive, Block 1, Courtyards at the Oaks 2 <span>nd </span> Addition to the City
of Derby, Sedgwick County, Kansas.</a></p>
<p></p>
<p>(d)  The
method of assessment is:  <span>Equally per lot (12
lots).</span></p>
<p></p>
<p>(e)  The apportionment of the cost of the
Improvements between the Improvement District and the City-at-large is:  100%
to be assessed against the Improvement District and 0% to be paid by the
City-at-large.</p>
<p></p>
<p><b><span>Section 2 </span></b>.  <b><span>Authorization of Improvements </span></b>.  The
Improvements are hereby authorized and ordered to be made in accordance with
the findings of the governing body of the City as set forth in <b><i><span>Section 1 </span></i></b> of this Resolution.</p>
<p></p>
<p><b><span>Section 3 </span></b>.  <b><span>Bond Authority; Reimbursement </span></b>.  The Act
provides for the costs of the Improvements, interest on interim financing and
associated financing costs to be paid by the issuance of general obligation
bonds or special obligation bonds of the City (the Bonds).  The Bonds may be
issued to reimburse expenditures made on or after the date which is 60 days
before the date of this Resolution, pursuant to Treasury Regulation 1.150-2.</p>
<p></p>
<p><b><span>Section 4 </span></b>.  <b><span>Effective Date </span></b>.  This Resolution shall
be effective upon adoption.  This Resolution shall be published one time in the
official City newspaper, and shall also be filed of record in the office of the
Register of Deeds of Sedgwick County, Kansas.</p>
<p></p>
<p></p>
<p></p>
</div>
</body>`;

const remove =(arr)=>{
    for (var i = arr. length - 1; i >= endpoint; i--) {
        arr[i].textContent.trim() === "" && arr[i].parentNode.removeChild(arr[i]);
        }
        str = root.innerHTML; 
        console.log(str);

}
const removeTagProcess = (arr)=>{
    return new Promise((resolve, reject) => {
        if(arr){
            resolve(
            remove(arr)
          
            );
        }
        else reject('something went wrong');

    });
}
let root = HTMLParser.parse(htmlContent);
let arr= root.querySelectorAll("p");
let endpoint = arr.length-4;
const removeEmptyTags = async () => {  
    try {
      
      await removeTagProcess(arr,endpoint);
      console.log("Sucess!");
    } catch (err) {
      // catches errors
      console.log(err);
    }
  };
  
  removeEmptyTags();
  //---write file using async / await ends---//
// root.querySelectorAll("p")
//     .forEach(el => el.textContent.trim() === "" && el.parentNode.removeChild(el))
// str = root.innerHTML; 
// console.log(str);
